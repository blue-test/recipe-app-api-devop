variable "aws_region" {
  default = "us-east-1"
}

variable "prefix" {
  default = "read"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "blue@mailc.net"
}

variable "db_username" {
  description = "Username for the RDS postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}

variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "163601644276.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-devops"
}

variable "ecr_image_proxy" {
  description = "ECR image for proxy"
  default     = "163601644276.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-proxy"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}